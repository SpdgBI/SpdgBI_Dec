package SpgBI_Dec.Biz;

import SpgBI_Dec.Common.DBUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ExchangeLogs {
    public static String GetExchangeLog(String VoucherID){
        Connection connERP = null;
        PreparedStatement pstmtExchangelog = null;
        ResultSet rsExchangelog = null;
        StringBuffer strSqlExchangelog = null;
        String rsJson = "";
        JSONArray array = null;
        try {
            connERP = DBUtils.getConnection("ERP");
            strSqlExchangelog = new StringBuffer();
            strSqlExchangelog.append("SELECT VOUCHERID,LOGMESSAGE FROM cw_xzd_yspz_logs WHERE VOUCHERID = ?");
            pstmtExchangelog = connERP.prepareStatement(strSqlExchangelog.toString());
            pstmtExchangelog.setString(1,VoucherID);
            rsExchangelog = pstmtExchangelog.executeQuery();
            array = new JSONArray();
            JSONObject mapOfColValues = new JSONObject();
            if (rsExchangelog.next()){

                mapOfColValues.put("MESSAGE",rsExchangelog.getString("LOGMESSAGE"));
                mapOfColValues.put("FLAG","SUCCESS");

            }
            else{
                mapOfColValues.put("MESSAGE","");
                mapOfColValues.put("FLAG","FAIL");

            }
            array.put(mapOfColValues);


        }catch (Exception ex){
            rsJson = "";
            ex.printStackTrace();
        }finally {
            rsJson = array.toString();
            DBUtils.close(connERP);
            return rsJson;
        }
    }
}
