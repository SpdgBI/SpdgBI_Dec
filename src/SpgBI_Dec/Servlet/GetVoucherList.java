package SpgBI_Dec.Servlet;

import SpgBI_Dec.Biz.Voucher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet(name = "GetVoucherList")
public class GetVoucherList extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        response.setCharacterEncoding("UTF-8");
        String strUnitList = request.getParameter("unitlist");
        String strPeriod = request.getParameter("period");
        String strStatus = request.getParameter("status");
        String userJson = Voucher.GetVoucherList(strUnitList,strPeriod,strStatus);
        OutputStream out = response.getOutputStream();
        out.write(userJson.getBytes("UTF-8"));
        out.flush();
    }
}
