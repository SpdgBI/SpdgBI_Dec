/**
 * Created by Administrator on 2017/9/9.
 */
Ext.define('SpdgbiEtl.proxy.Api', {


    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.api',

    reader: {
        type: 'json',
        rootProperty: 'data'
    }

});