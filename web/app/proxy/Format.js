/**
 * Created by hxlx on 2017-2-7.
 */
Ext.define('SpdgbiEtl.proxy.Format', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.format',
    requires:[
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],
    actionMethods: {
        create : 'POST',
        read   : 'GET', // by default GET
        update : 'POST',
        destroy: 'POST'
    },

    reader: {
        method: 'post',
        type: 'json',
        rootProperty: 'data',
        messageProperty: 'msg'
    },

    writer:{
        type: 'json',
        root: 'data',
        encode: true,
        allowsingle: true
    }


});