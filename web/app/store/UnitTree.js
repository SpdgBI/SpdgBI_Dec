/**
 * Created by hxlx on 2017/2/5.
 */
Ext.define('SpdgbiEtl.store.UnitTree', {
    extend: 'Ext.data.TreeStore',
    alias: 'store.unittree',
    storeId: 'UnitTree',
    requires: [
        'SpdgbiEtl.proxy.Format'
    ],
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'app/data/unittree.json',
        reader: {
            type: 'json',
            rootProperty: 'children'
        }
        /*
         extraParams: {
         a: 2
         },
         */
    },
    root: {
        id: -1,
        text: '公司目录',
        expanded: false,
        iconCls: 'x-fa fa-home'
    },
    sorters: [{
        property: 'UNITCODE',
        direction: 'ASC'
    }],

    //Fields can also be declared without a model class:
    fields: [
        {name: 'UNITCODE', type: 'string'},
        {name: 'UNITNAME', type: 'string'}
    ]


});