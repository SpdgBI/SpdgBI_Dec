/**
 * Created by hxlx on 2017/1/15.
 */
Ext.define('SpdgbiEtl.view.findatapump.FindataPump', {
    extend: 'Ext.panel.Panel',

    requires: [
        'SpdgbiEtl.view.findatapump.FindataPumpModel',
        'SpdgbiEtl.view.findatapump.FindataPumpController'
    ],


    //Uncomment to give this component an xtype
    xtype: 'findatapumpPanel',

    layout: {
        type: 'fit',
        pack: 'start',
        align: 'stretch'
    },
    //width: 500,
    height: 500,
    bodyPadding: 10,

    viewModel: {
        type: 'findatapump'
    },

    controller: 'findatapump',

    items: [
        {
            xtype: 'gridpanel',
            flex: 1,
            //border: true,
            title: '财务数据抽取',
            reference: 'replistGrid',
            tbar: [
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    name: 'unit',
                    editable: false,
                    //readOnly: false,
                    hideLabel: true,
                    allowBlank: false,
                    bind: {
                        emptyText: '{txtEmpty}',
                        value: '{lstUnitBrief}'
                    },

                    triggers: {
                        glyphed: {
                            cls: 'x-fa fa-search',
                            handler: 'onUnitSelect'
                        }
                    }
                },
                '-',
                {
                    xtype: 'label',
                    text: '开始日期：'
                },
                {
                    xtype: 'datefield',
                    anchor: '100%',
                    name: 'date',
                    reference: 'startdateDf',
                    format: 'Y-m-d'
                    //value: '2 4 1978'
                },
                {
                    xtype: 'label',
                    text: '结束日期：'
                },
                {
                    xtype: 'datefield',
                    anchor: '100%',
                    reference: 'enddateDf',
                    name: 'date',
                    format: 'Y-m-d',
                    altFormats: 'm,d,Y|m.d.Y'
                    //value: '2.4.1978'
                },
                '-',
                {
                    text: '报表类型',
                    width: 150,
                    textAlign: 'left',
                    reference: 'quartersButton',
                    menu: {
                        id: 'replistmenu',
                        cls: 'pl-option-menu',
                        items: [
                            {
                                text: '资产负债表',
                                checked: true       // when checked has a boolean value, it is assumed to be a CheckItem
                            },
                            {
                                text: '利润表',
                                checked: true       // when checked has a boolean value, it is assumed to be a CheckItem
                            },
                            {
                                text: '现金流量表',
                                checked: true       // when checked has a boolean value, it is assumed to be a CheckItem
                            },

                            {
                                text: '成本费用表',
                                checked: true       // when checked has a boolean value, it is assumed to be a CheckItem
                            }
                        ]
                    }
                },
                '-',
                {
                    xtype: 'button',
                    reference: 'GetlistButton',
                    scale: 'small',
                    width: 100,
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '查询',
                    listeners: {
                        click: 'onGetlistClick'
                    }
                }
            ],
            bbar: [
                {
                    xtype: "pagingtoolbar",
                    bind: {
                        store: '{finreplist}'
                    },
                    displayInfo: true
                },
                '->',
                {
                    xtype: 'button',
                    reference: 'ExpButton',
                    scale: 'small',
                    width: 100,
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '列表导出',
                    listeners: {
                        click: 'onExpClick'
                    }
                },
                {
                    xtype: 'button',
                    reference: 'PumpButton',
                    scale: 'small',
                    width: 100,
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '抽取数据',
                    listeners: {
                        click: 'onPumpClick'
                    }
                }
            ],
            bind: {
                store: '{finreplist}'
            },
            columnLines: true,
            selModel: {
                type: 'checkboxmodel',
                checkOnly: false
            },
            columns: [
                {text: '年份', dataIndex: 'DIMYEAR', flex: 1, align: 'center'},
                {text: '月份', dataIndex: 'DIMMONTH', flex: 1, align: 'center'},
                {text: '报表名称', dataIndex: 'REPNAME', flex: 2, align: 'center'},
                {text: '公司名称', dataIndex: 'UNITNAME', flex: 2, align: 'center'},
                {text: '上报时间', dataIndex: 'CFMDATE', flex: 1, align: 'center'},
                {text: '抽取时间', dataIndex: 'PUMPDATE', flex: 1, align: 'center'}
            ]
        }
    ]
});