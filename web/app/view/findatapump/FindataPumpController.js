/**
 * Created by hxlx on 2017/1/15.
 */
Ext.define('SpdgbiEtl.view.findatapump.FindataPumpController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.findatapump',

    /**
     * Called when the view is created
     */
    init: function () {
        this.getView().getViewModel().set('lstRep', ['FIN001', 'FIN002', 'FIN003', 'FIN005']);
    },

    onUnitSelect: function () {
        this.showUnitWindow();
    },

    onExpClick: function () {
        SpdgbiEtl.ux.udf.grid2Excel(this.lookupReference('ReplistGrid'), 'abc.xls');
    },

    showUnitWindow: function () {
        var UnitWin = Ext.create('SpdgbiEtl.view.unitselect.Unitselect', {getUnitTree: this.getUnitTree, scope: this});
        UnitWin.show();
    },

    getUnitTree: function (pUnitname, pUnitcode) {
        this.getView().getViewModel().set('lstUnitname', pUnitname);
        this.getView().getViewModel().set('lstUnitcode', pUnitcode);
    },

    onPumpClick: function () {
        /*        Ext.Ajax.request({
         url : "'server/findatapump/GetEtlfinrep.php?data=".this.getView().getViewModel().get('lstUnit'),
         method : 'POST',
         success : function(response) {
         var json = Ext.JSON.decode(response.responseText);

         //column = new Ext.grid.column.Column(json.columModle);
         //alert(json.data[2].id);
         var store = Ext.create('Ext.data.Store', {
         fields : json.fields,
         data : json.data
         });

         Ext.getCmp("grid_a").reconfigure(store, json.columns);

         }
         });*/
        var grid = this.getView().lookupReference('replistGrid'),
            records = grid.getSelectionModel().getSelection(),
            names = [],
            codes = [];


        Ext.Array.each(records, function (rec) {
            names.push(rec.get('UNITNAME'));
            codes.push(rec.get('UNITCODE'));
        });

        Ext.MessageBox.show({
         title: '您选择了以下单位：',
         msg: names.join('<br />'),
         icon: Ext.MessageBox.INFO
         });
    },
    onGetlistClick: function () {
        var store = this.getView().getViewModel().getStore('finreplist'),
            proxy = store.getProxy();
        proxy.extraParams = {
            "unitlist": this.getView().getViewModel().get('lstUnitcode').join(','),
            "replist": this.getView().getViewModel().get('lstRep').join(','),
            "startdate": this.getView().lookupReference('startdateDf').value,
            "enddate": this.getView().lookupReference('enddateDf').value
            //dataType : "json"
        };
        store.load();
        //this.getView().getViewModel().getStore('finreplist').load({params:{"name":'张三',"age":18}});
    }
})
;