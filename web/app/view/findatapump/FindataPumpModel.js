/**
 * Created by hxlx on 2017/1/15.
 */
Ext.define('SpdgbiEtl.view.findatapump.FindataPumpModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.findatapump',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'FindataPump',
            autoLoad: true
        }
        */
        finreplist: {
            fields: [
                'DIMYEAR','DIMMONTH','REPCODE','REPNAME', 'UNITCODE','UNITNAME', 'CFMFLAG','CFMDATE','PUMPFLAG','PUMPDATE'
            ],
            pageSize: 25,
            //data: {},
            autoLoad: false,
            proxy: {
                type: 'format',
                api: {
                    read: 'server/findatapump/getFinrepList.php'
                },
                reader: {
                    rootProperty: 'items'
                }
            }            
        }
    },

    data: {
        //lstUnit:'海峡出版发行集团，新华发行集团，福州分公司',
        lstUnitname:[],
        lstUintcode:[],
        lstRep:[]
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    },

    formulas: {
        // We'll explain formulas in more detail soon.
        lstUnitBrief: function (get) {
            var fn = get('lstUnitname').join(',');
            return (fn=="")?null:fn.substr(0,8) + '....';
        },
        txtEmpty: function (get) {
            return get('lstUnit') ? '请选择单位':'';
        }
    }
});