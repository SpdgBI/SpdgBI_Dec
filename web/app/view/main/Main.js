/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SpdgbiEtl.view.main.Main', {
    extend: 'Ext.container.Viewport',

    xtype: 'app-main',

    requires: [
        'Ext.button.Segmented',
        'Ext.window.MessageBox',

        'SpdgbiEtl.view.main.MainController',
        'SpdgbiEtl.view.main.MainModel',
        'SpdgbiEtl.view.main.List'
    ],

    controller: 'main',
    viewModel: 'main',

    cls: 'sencha-dash-viewport',
    itemId: 'mainView',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },


    items: [
        {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar shadow',
            height: 64,
            itemId: 'headerBar',
            items: [
                {
                    iconCls:'x-fa fa-navicon',
                    ui: 'header',
                    href: '#searchresults',
                    hrefTarget: '_self',
                    tooltip: 'See latest search'
                },
                {
                    xtype: 'tbtext',

                    ui: 'header',
                    id: 'app-header-title',
                    text: '福建新华发行集团财务数据交换平台'
                },
                /*
                {
                    xtype: 'tbtext',
                    reference: 'senchaLogo',
                    id: 'app-header-title',
                    iconCls: 'fa-th-list',
                    text: '福建新华发行集团财务数据交换平台'
                },

                {
                    margin: '0 0 0 8',
                    ui: 'header',
                    iconCls:'x-fa fa-navicon',
                    id: 'main-navigation-btn',
                    handler: 'onToggleNavigationSize'
                },
                */
                '->',
                {
                    iconCls:'x-fa fa-search',
                    ui: 'header',
                    href: '#searchresults',
                    hrefTarget: '_self',
                    tooltip: 'See latest search'
                },
                {
                    iconCls:'x-fa fa-envelope',
                    ui: 'header',
                    href: '#email',
                    hrefTarget: '_self',
                    tooltip: 'Check your email'
                },
                {
                    iconCls:'x-fa fa-question',
                    ui: 'header',
                    href: '#faq',
                    hrefTarget: '_self',
                    tooltip: 'Help / FAQ\'s'
                },
                {
                    iconCls:'x-fa fa-th-large',
                    ui: 'header',
                    href: '#profile',
                    hrefTarget: '_self',
                    tooltip: 'See your profile'
                }
            ]
        },
        {"xtype": 'tabpanel',
            ui: 'navigation',

            titleRotation: 0,
            tabRotation: 0,
            tabPosition: 'left',
            flex:1,

            tabBar: {
                flex: 1,
                layout: {
                    align: 'left',
                    overflowHandler: 'none'
                }
            },

            responsiveConfig: {
                tall: {
                    headerPosition: 'top'
                },
                wide: {
                    headerPosition: 'left'
                }
            },

            defaults: {
                bodyPadding: 5
            },

            items: [{
                title: '财务数据交换',
                iconCls: 'fa-home',
                layout: {
                    type: 'fit',
                    align: 'left'
                },
                items: [{
                    xtype: 'voucherexchange'
                }]
            }, {
                title: '交换日志查看',
                iconCls: 'fa-user',
                bind: {
                    html: '{loremIpsum}'
                }
            },
                {
                    title: '财务业务对账',
                    iconCls: 'fa-cog',
                    bind: {
                        html: '{loremIpsum}'
                    }
                },
                {
                    title: '参数设置',
                    iconCls: 'fa-cog',
                    bind: {
                        html: '{loremIpsum}'
                    }
                }]
        }
        ]
});






