/**
 * Created by hxlx on 2017/1/30.
 */
Ext.define('SpdgbiEtl.view.unitselect.Unitselect', {
    extend: 'Ext.window.Window',

    requires: [
        'SpdgbiEtl.view.unitselect.UnitselectModel',
        'SpdgbiEtl.view.unitselect.UnitselectController',
        'Ext.layout.container.VBox',
        'Ext.tree.Panel'
    ],

    xtype: 'unitselect',
    title: '选择单位',
    closable: false,
    autoshow: true,
    height: 400,
    width: 500,
    bodyPadding: 5,
    modal: true,
    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'

    },


    viewModel: {
        type: 'unitselect'
    },

    controller: 'unitselect',

    items: [
        {
            xtype: 'toolbar',
            height: 38,
            margin: '0 0 10 0',
            items: [
                {
                    xtype:'textfield',
                    name: 'searchfield',
                    emptyText: '搜索'
                },
                '->',
                {
                    xtype: 'button',
                    reference: 'RefreshButton',
                    scale: 'small',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '刷新',
                    listeners: {
                        click: 'onRefreshClick'
                    }
                },
                {
                    xtype: 'button',
                    reference: 'SelectAllButton',
                    scale: 'small',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '全选'

                }

            ]
        },
        {
            xtype: 'treepanel',
            //width: 400,
            //height: 200,
            reference: 'tpUnitTree',
            flex: 1,
            frame: true,
            rootVisible: true,
            //bind: '{treeitems}'
            store: 'UnitTree'
            //checkPropagation : 'down'

        },
        {
            xtype: 'toolbar',
            height: 38,
            margin: '0 0 10 0',
            items: [
                '->',
                {
                    xtype: 'button',
                    reference: 'confirmButton',
                    scale: 'small',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '确定',
                    listeners: {
                        click: 'onConfirmClick'
                    }
                },
                {
                    xtype: 'button',
                    reference: 'CancelButton',
                    scale: 'small',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '取消',
                    listeners: {
                        click: 'onCancelClick'
                    }
                }

            ]

        }

        /* include child components here */
    ]
})
;