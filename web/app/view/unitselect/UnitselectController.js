/**
 * Created by hxlx on 2017/1/30.
 */
Ext.define('SpdgbiEtl.view.unitselect.UnitselectController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.unitselect',

    /**
     * Called when the view is created
     */
    init: function() {
        this.lookupReference('tpUnitTree').getStore().getRoot().expand();
    },

    onConfirmClick: function (button){
        var records = this.lookupReference('tpUnitTree').getChecked(),
            names = [],
            codes = [];


        Ext.Array.each(records, function(rec){
            names.push(rec.get('text'));
            codes.push(rec.get('UNITCODE'));
        });

/*        Ext.MessageBox.show({
            title: '您选择了以下单位：',
            msg: names.join('<br />'),
            icon: Ext.MessageBox.INFO
        });*/

        //console.log(names);
        var win = button.up('window');

        Ext.callback(win.getUnitTree,win.scope,[names,codes]);
        this.getView().close();
    },

    onRefreshClick: function () {
        this.lookupReference('tpUnitTree').getStore().reload();
    },

    onCancelClick: function () {
        this.getView().close();
    }

});