/**
 * Created by hxlx on 2017/1/30.
 */
Ext.define('SpdgbiEtl.view.unitselect.UnitselectModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.unitselect',
    stores: {
        //treeitems : 'unittree'
    },


        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'Unitselect',
            autoLoad: true
        }
        */

    data: {
        unit : 'abc'
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});