/**
 * Created by lixin on 2017/11/2.
 */
Ext.define('SpdgbiEtl.view.voucherexchange.VoucherBody', {
    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.container.Container',
        'Ext.form.field.HtmlEditor',
        'Ext.layout.container.Anchor',
        'Ext.layout.container.HBox'
    ],


    xtype: 'voucherbody',



    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true,
        enableTextSelection: true

    },

    //flex: 1,
    //border: true,


    title: '凭证明细数据',
    reference: 'voucherbodyGrid',
    //headerBorders: false,
    rowLines: true,
    columnLines: true,
    scrollable: true,


    tbar: [
        // Default item type for toolbar is button, thus we can skip it's definition in
        // the array items
        {
            iconCls: 'x-fa fa-angle-left',
            listeners: {
                click: 'onBackBtnClick'
            }
        },
        {
            iconCls: 'x-fa fa-trash'
        },
        {
            iconCls: 'x-fa fa-exclamation-circle',
            listeners: {
                click: 'onGetLogClick'
            }
        },
        {
            iconCls:'x-fa fa-print'
        },
        {
            iconCls: 'x-fa fa-forward'
        }
    ],

    bbar: {
        defaults: {
            margin:'0 15 0 0'
        },
        items: [

            '->',

            {
                xtype: 'button',
                reference: 'BackButton',
                scale: 'small',
                width: 100,
                iconAlign: 'right',
                iconCls: 'x-fa fa-angle-right',
                text: '数据交换',
                listeners: {
                    click: 'onExchangeDataClick'
                }
            }
        ]
    },
    bind: {
        store: '{voucherbody}'
    },
    

    columns: [
        {text: '分录', dataIndex: 'FLOWID', flex: 0.5, align: 'center'},
        {text: '摘要', dataIndex: 'ZY', flex: 2, align: 'center'},
        {text: '科目代码', dataIndex: 'KMDM', flex: 1, align: 'center'},
        {text: '辅助标志', dataIndex: 'FZBZ', flex: 0.5, align: 'center'},
        {text: '客商代码', dataIndex: 'DWDM', flex: 1, align: 'center'},
        {text: '集团内外辅助信息', dataIndex: 'JTNWFZ', flex: 1, align: 'center'},
        {text: '币种', dataIndex: 'BZ', flex: 0.5, align: 'center'},
        {text: '借方', dataIndex: 'DEBITAMOUNT', flex: 1, align: 'center'},
        {text: '贷方', dataIndex: 'CREDITAMOUNT', flex: 1, align: 'center'}

    ]
});