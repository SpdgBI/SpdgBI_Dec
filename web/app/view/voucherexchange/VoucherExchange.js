/**
 * Created by lixin on 2017/10/29.
 */
Ext.define('SpdgbiEtl.view.voucherexchange.VoucherExchange', {
    extend: 'Ext.Container',

    requires: [
        'SpdgbiEtl.view.voucherexchange.VoucherExchangeModel',
		'SpdgbiEtl.view.voucherexchange.VoucherExchangeController'
    ],


    xtype: 'voucherexchange',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    viewModel: {
        type: 'voucherexchange'
    },

    controller: 'voucherexchange',

    items: [
        /* include child components here */
        {
            xtype: 'container',
            itemId: 'contentPanel',
            margin: '0 20 20 0',
            flex: 1,
            layout: 'fit'
        }
    ]
});