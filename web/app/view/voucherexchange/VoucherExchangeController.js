/**
 * Created by lixin on 2017/10/29.
 */
Ext.define('SpdgbiEtl.view.voucherexchange.VoucherExchangeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.voucherexchange',

    /**
     * Called when the view is created
     */
    init: function() {
        this.setCurrentView('voucherheader');
    },

    onBackBtnClick: function() {
        this.setCurrentView('voucherheader');
    },

    onBackBodyBtnClick: function() {
        this.setCurrentView('voucherbody');
    },
    setCurrentView: function(view, params) {
        var contentPanel = this.getView().down('#contentPanel');

        //We skip rendering for the following scenarios:
        // * There is no contentPanel
        // * view xtype is not specified
        // * current view is the same
        if(!contentPanel || view === '' || (contentPanel.down() && contentPanel.down().xtype === view)){
            return false;
        }

        if (params && params.openWindow) {
            var cfg = Ext.apply({
                xtype: 'voucherheader',
                items: [
                    Ext.apply({
                        xtype: view
                    }, params.targetCfg)
                ]
            }, params.windowCfg);

            Ext.create(cfg);
        } else {
            Ext.suspendLayouts();

            contentPanel.removeAll(true);
            contentPanel.add(
                Ext.apply({
                    xtype: view
                }, params)
            );

            Ext.resumeLayouts(true);
        }
    },

    onGridCellItemClick: function(view, td, cellIndex, record){
        var store = this.getView().getViewModel().getStore('voucherbody'),
            voucherid = record.get('SAMEVOUCHER'),
            proxy = store.getProxy();
        proxy.extraParams = {
            "voucherid": voucherid
                //Math.floor(Math.random()*10)
        };
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                if (success) {
                    if(cellIndex > 1){
                        this.setCurrentView('voucherbody', {record: record});
                    } else if (cellIndex === 1) {
                        //Invert selection
                        record.set('favorite', !record.get('favorite'));
                    }

                    //Ext.MessageBox.alert('提示', msg.join('<br />'));
                }else{
                    Ext.MessageBox.alert('提示', '出错了');
                }
            }
        });



    },

    beforeDetailsRender: function(view) {
       // var record = view.record ? view.record : {};

        //view.down('#mailBody').setHtml(record.get('contents'));
       // view.down('#attachments').setData(record.get('attachments'));
       // view.down('#emailSubjectContainer').setData(record.data? record.data: {});
       // view.down('#userImage').setSrc('resources/images/user-profile/'+ record.get('user_id') + '.png');
    },

    onGetlistClick: function () {
        var store = this.getView().getViewModel().getStore('voucherlist'),
            proxy = store.getProxy();

        proxy.extraParams = {
            "unitlist": this.getView().getViewModel().get('lstUnitcode').join(','),
            "status": (this.getView().getViewModel().get('SuccStatus') + this.getView().getViewModel().get('FailStatus')).toString(),
            "period": this.getView().lookupReference('voucherdateDf').value
            //dataType : "json"
        };
        store.load();
        //this.getView().getViewModel().getStore('finreplist').load({params:{"name":'张三',"age":18}});
    },

    onExchangeDataClick: function(){
        var storelist = this.getView().getViewModel().getStore('voucherlist'),
            storebody = this.getView().getViewModel().getStore('voucherbody'),
            voucherid = storebody.getAt(0).get('SAMEVOUCHER');
        Ext.Ajax.request({
            url: 'biz/ExchangeVoucher',
            scope: this,
            headers: {
                'userHeader': 'userMsg'
            },
            params: { voucherid: voucherid },
            method: 'GET',
            success: function (response, options) {
               // Ext.MessageBox.alert('完成', '当前数据交换完成，请从日志中查看完成情况。' + response.responseText);
                Ext.MessageBox.alert('完成', '当前数据交换完成，请从日志中查看完成情况。');
                storelist.reload();
                this.setCurrentView('voucherheader');
            },
            failure: function (response, options) {
                //Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
                Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
            }
        });

    },

    onGetVoucherBodyClick: function () {
        var store = this.getView().getViewModel().getStore('voucherbody'),
            proxy = store.getProxy();

        proxy.extraParams = {
            "voucherid": 'F21174404'
            //dataType : "json"
        };
        store.load();
        //this.getView().getViewModel().getStore('finreplist').load({params:{"name":'张三',"age":18}});
    },

    onUnitSelect: function () {
        this.showUnitWindow();
    },



    showUnitWindow: function () {
        var UnitWin = Ext.create('SpdgbiEtl.view.unitselect.Unitselect', {getUnitTree: this.getUnitTree, scope: this});
        UnitWin.show();
    },

    getUnitTree: function (pUnitname, pUnitcode) {
        this.getView().getViewModel().set('lstUnitname', pUnitname);
        this.getView().getViewModel().set('lstUnitcode', pUnitcode);
    },

    onItemCheck: function(item, checked){
        if (item.text == '成功'){
            this.getView().getViewModel().set('SuccStatus',checked ? 1:0)
        }
        else{
            this.getView().getViewModel().set('FailStatus',checked ? 2:0)
        }
    },

    onGetLogClick: function(){

        var storebody = this.getView().getViewModel().getStore('voucherbody'),
            voucherid = storebody.getAt(0).get('SAMEVOUCHER');

        Ext.Ajax.request({
            url: 'biz/GetExchangeLog',
            scope: this,
            headers: {
                'userHeader': 'userMsg'
            },
            params: { voucherid: voucherid },
            method: 'GET',
            success: function (response, options) {
                // Ext.MessageBox.alert('完成', '当前数据交换完成，请从日志中查看完成情况。' + response.responseText);
                var obj = Ext.decode(response.responseText)
                    flag = obj[0].FLAG;
                if (flag == "SUCCESS") {
                    //this.getView().getViewModel().set('Log', obj[0].MESSAGE.replace(/\n/gi, '<br/>'));
                    this.getView().getViewModel().set('Log', obj[0].MESSAGE);
                    this.setCurrentView('voucherlog');
                }
            },
            failure: function (response, options) {
                //Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
                Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
            }
        });
    }

});