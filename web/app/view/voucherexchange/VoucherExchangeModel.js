/**
 * Created by lixin on 2017/10/29.
 */
Ext.define('SpdgbiEtl.view.voucherexchange.VoucherExchangeModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.voucherexchange',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'FindataPump',
            autoLoad: true
        }
        */
        voucherlist: {
            fields: [
                'NCUSER','VOUCHERDATE','PK_CORP','ACCOUNTING_PERIOD', 'TOTALDEBIT','TOTALCREDIT', 'FISCAL_YEAR','REMARK','SAMEVOUCHER','ISREAD','ISSUCCEED'
            ],
            //data: {},
            autoLoad: false,
            proxy: {
                type: 'format',
                api: {
                    read: 'biz/GetVoucherList'
                },
                reader: {
                    rootProperty: 'items'
                }
            }
        },
        voucherbody: {
            fields: [
                'SAMEVOUCHER','FLOWID','ZY','KMDM','FZBZ','DWDM','JTNWFZ','BZ','DEBITAMOUNT','CREDITAMOUNT'
            ],
            autoload:false,
            proxy: {
                type: 'format',
                api: {
                    read: 'biz/GetVoucherBody'
                },
                reader: {
                    rootProperty: 'items'
                }
            }

        }
    },

    data: {
        //lstUnit:'海峡出版发行集团，新华发行集团，福州分公司',
        lstUnitname:[],
        lstUintcode:[],
        SuccStatus: 1,
        FailStatus: 2,
        Log: '1222222222222221111'
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    },

    formulas: {
        // We'll explain formulas in more detail soon.
        lstUnitBrief: function (get) {
            var fn = get('lstUnitname').join(',');
            return (fn=="")?null:fn.substr(0,8) + '....';
        },
        txtEmpty: function (get) {
            return get('lstUnit') ? '请选择单位':'';
        }
    }
});