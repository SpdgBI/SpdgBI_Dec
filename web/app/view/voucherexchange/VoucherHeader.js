/**
 * Created by lixin on 2017/11/2.
 */
Ext.define('SpdgbiEtl.view.voucherexchange.VoucherHeader', {
    extend: 'Ext.grid.Panel',


    xtype: 'voucherheader',


    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true,
        enableTextSelection: true
    },

    //flex: 1,
    //border: true,

    reference: 'voucherlistGrid',
    //headerBorders: false,
    rowLines: false,
    scrollable: true,

    tbar: [
        {
            xtype: 'textfield',
            cls: 'auth-textbox',
            name: 'unit',
            editable: false,
            //readOnly: false,
            hideLabel: true,
            allowBlank: false,
            bind: {
                emptyText: '{txtEmpty}',
                value: '{lstUnitBrief}'
            },

            triggers: {
                glyphed: {
                    cls: 'x-fa fa-search',
                    handler: 'onUnitSelect'
                }
            }
        },
        '-',
        {
            xtype: 'label',
            text: '会计日期：'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            name: 'date',
            reference: 'voucherdateDf',
            format: 'Y-m'
            //value: '2 4 1978'
        },
        '-',
        {
            text: '交换状态',
            width: 150,
            textAlign: 'left',
            reference: 'quartersButton',
            menu: {
                id: 'exchagestatusmenu',
                cls: 'pl-option-menu',
                items: [
                    {
                        text: '交换成功',
                        checked: true,
                        checkHandler: 'onItemCheck'
                    },
                    {
                        text: '交换失败',
                        checked: true,
                        checkHandler: 'onItemCheck'
                    },
                    {
                        text: '未交换',
                        checked: true,
                        checkHandler: 'onItemCheck'
                    }
                ]
            }
        },
        '-',
        {
            xtype: 'button',
            reference: 'GetlistButton',
            scale: 'small',
            width: 100,
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: '查询',
            listeners: {
                click: 'onGetlistClick'
            }
        }
    ],
    bbar: [
        '->',
        {
            xtype: 'textfield',
            text: '批次号：'
        },
        {
            xtype: 'button',
            reference: 'ExpButton',
            scale: 'small',
            width: 100,
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: '搜索',
            listeners: {
                click: 'onExpClick'
            }
        },
        '-',
        {
            xtype: 'button',
            reference: 'PumpButton',
            scale: 'small',
            width: 100,
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: '清空查询条件',
            listeners: {
                click: 'onPumpClick'
            }
        }
    ],
    bind: {
        store: '{voucherlist}'
    },
    columnLines: true,
    selModel: {
        type: 'checkboxmodel',
        checkOnly: false
    },
    listeners: {
        celldblclick: 'onGridCellItemClick'
    },
    columns: [
        {text: '主体账簿', dataIndex: 'PK_CORP', flex: 1, align: 'center'},
        {text: '凭证日期', dataIndex: 'VOUCHERDATE', flex: 1, align: 'center'},
        {text: '年度', dataIndex: 'FISCAL_YEAR', flex: 0.5, align: 'center'},
        {text: '期间', dataIndex: 'ACCOUNTING_PERIOD', flex: 0.5, align: 'center'},
        {text: '借方合计', dataIndex: 'TOTALDEBIT', flex: 1, align: 'center'},
        {text: '贷方合计', dataIndex: 'TOTALCREDIT', flex: 1, align: 'center'},
        {text: '批次号', dataIndex: 'SAMEVOUCHER', flex: 1, align: 'center'},
        {text: '检验状态', dataIndex: 'ISREAD', flex: 0.5, align: 'center',
           // renderer: function(value) {
           //     return '<span class="x-fa fa-heart'+ (value ? 'N' : '-o') +'"></span>';
           // }
        },
        {text: '交换状态', dataIndex: 'ISSUCCESS', flex: 0.5, align: 'center'},
        {text: '制单人', dataIndex: 'NCUSER', flex: 0.5, align: 'center'}

    ]

});