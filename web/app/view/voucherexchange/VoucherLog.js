/**
 * Created by Administrator on 2017/9/19.
 */
Ext.define('SpdgbiEtl.view.voucherexchange.VoucherLog', {
    extend: 'Ext.panel.Panel',


    xtype: 'voucherlog',

    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
    items: [
        /* include child components here */
        {
            xtype: 'textareafield',
            readonly: false,
            bind:{
                value: '{Log}'
            },
            flex:1

        },
        {
            xtype: 'toolbar',
            height: 38,
            margin: '0 0 10 0',
            flex:1,
            items: [
                '->',
                {
                    xtype: 'button',
                    reference: 'confirmButton',
                    scale: 'small',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '返回',
                    listeners: {
                        click: 'onBackBodyBtnClick'
                    }
                }

            ]

        }
    ]
});